from django.urls import path
from . import views

urlpatterns = [

    path('',views.connect,name="connect"),
    path('/inscription',views.inscription,name="inscription"),
    path('/quitter',views.logoutUser,name="quitter"),
]
