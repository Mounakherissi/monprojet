from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from .form import CreerUtilisateur
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

# Create your views here.

def inscription(request):
    form=CreerUtilisateur()
    if request.method=='POST':
        form=CreerUtilisateur(request.POST)
        if form.is_valid():
            form.save()
            user=form.cleaned_data.get('username')
            messages.success(request,'compe créé avec succés pour '+user)

            return redirect('connect')
    context={'form':form}

    return render(request,'connexion/inscription.html',context)


def connect(request):
    context={}
    if request.method=='POST':
        username=request.POST.get('username')
        password=request.POST.get('password1')
        user=authenticate(request,username=username,password=password)
        if user is not None:
            login(request,user)
            return redirect('home')
        else:
            messages.info(request,"Erreur dans le nom d'utilisateur et/ou mot de passe")

    return render(request,'connexion/connect.html',context)

def logoutUser(request):
    logout(request)
    return redirect('connect')