from django.db import models

# Create your models here.

class Formation(models.Model):
    CATEGORIE=(('web','web'),
            ('mobile','mobile'),
              ('cloud', 'cloud'),
              ('securité web', 'securité web'),
            ('machine learning','machine learning'))
    ETAT=(('activé','activé'),
            ('non activé','non activé'))


    titre=models.CharField(max_length=200,null=True)
    logo=models.ImageField(null=True,blank=True,upload_to='images/')
    categorie = models.CharField(max_length=200, null=True,choices=CATEGORIE)
    etat = models.CharField(max_length=200, null=True, choices=ETAT)

    class Meta:
        db_table = "Formation"

    def __str__(self):
        return self.titre
