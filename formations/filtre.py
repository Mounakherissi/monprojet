import django_filters
from .models import Formation


class FormationFiltre(django_filters.FilterSet):
    class Meta:
        model=Formation
        fields=['titre','categorie','etat']