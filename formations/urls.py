from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('',views.user, name="user"),
    path('home',views.home, name="home"),
    path('ajouter_formation/', views.ajouter_formation, name="ajouter_formation"),
    path('modifier_formation/<str:pk>', views.modifier_formation, name="modifier_formation"),
    path('supprimer_formation/<str:pk>', views.supprimer_formation, name="supprimer_formation")
]
