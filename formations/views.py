from django.shortcuts import render
from django.shortcuts import render,redirect
from .form import FormationForm
from .models import Formation
from django.contrib.auth.decorators import login_required
from .filtre import FormationFiltre
from django.http import HttpResponse

# Create your views here.


def user(request):
    formations=Formation.objects.all()
    myFilter=FormationFiltre(request.GET,queryset=formations)
    formations=myFilter.qs
    context={'formations':formations,'myFilter':myFilter}
    return render(request,'formations/user.html',context)

@login_required(login_url='connect')
def home(request):
    formations=Formation.objects.all()
    context={'formations':formations}
    return render(request,'formations/accueil.html',context)


@login_required(login_url='connect')
def ajouter_formation(request):
    form=FormationForm()
    if request.method=='POST':
        form=FormationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
    context={'form':form}
    return render(request,'formations/ajouter_formation.html',context)


@login_required(login_url='connect')
def modifier_formation(request,pk):
    formation=Formation.objects.get(id=pk)
    form=FormationForm(instance=formation)
    if request.method=='POST':
        form=FormationForm(request.POST,instance=formation)
        if form.is_valid():
            form.save()
            return redirect('home')
    context={'formation':formation}
    return render(request,'formations/modifier_formation.html',context)


@login_required(login_url='connect')
def supprimer_formation(request,pk):
    formation=Formation.objects.get(id=pk)
    if request.method=='POST':
        formation.delete()
        return redirect('home')
    context={'item':formation}
    return render(request,'formations/supprimer_formation.html',context)